class Component {
	tagName;
	children;
	attribute;

	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.attribute != null) {
			return this.renderAttribute();
		} else {
			return `<${this.tagName} />`;
		}
	}

	renderAttribute() {
		if (this.children == null) {
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
		} else {
			return `<${this.tagName} ${this.attribute.name}="${
				this.attribute.value
			}">${this.renderChildren()}</${this.tagName}>`;
		}
	}

	renderChildren() {
		if (this.children instanceof Array) {
			let all = '';

			this.children.forEach(element => {
				if (element instanceof Component) {
					all += element.render();
					console.log('Component Element');
				} else {
					all += element;
				}
			});

			return all;
		} else if (this.children instanceof Component) {
			return this.children.render();
		} else {
			return this.children;
		}
	}
}

export default Component;
