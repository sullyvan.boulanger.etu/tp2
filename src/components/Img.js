import Component from './Component.js';

class Img extends Component {
	constructor(img) {
		super('img', { name: 'src', value: img }, null);
	}
}
export default Img;
